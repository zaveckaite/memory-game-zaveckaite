const cards = document.querySelectorAll('.memory-card');

let hasFlippedCard = false;
let lockBoard = false;
let firstCard, secondCard;
let moves = 0;
let counter = document.querySelector(".moves");

function flipCard(){

    if(lockBoard) return;
    if(this === firstCard) return;
  this.classList.add('flip');
    
    if(!hasFlippedCard){
        //first click
        hasFlippedCard = true;
        firstCard = this;
        
        return;
    } 
        // second click
        secondCard = this;
        moveCounter();
        checkForMatch();
    
};

function checkForMatch(){
    if(firstCard.dataset.framework === secondCard.dataset.framework){
      disableCards();  
    }else{
        unflipCards();
    }
}
function disableCards(){
    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);
    
    resetBoard();
};
function unflipCards(){
    lockBoard = true;
    
    setTimeout(() => {
    firstCard.classList.remove('flip');
    secondCard.classList.remove('flip');
        
        resetBoard();
    }, 1500);
}
function resetBoard(){
    [hasFlippedCard, lockBoard] = [false, false];
    [firstCard, secondCard] = [null, null];
}

var second = 0, minute = 0; hour = 0;
var timer = document.querySelector(".timer");
var interval;

function startGame(){
    moves = 0;
    counter.innerHTML = moves;
}

function moveCounter(){
    moves++;
    counter.innerHTML = moves;
}
(function shuffle(){
    cards.forEach(card => {
       let randomPos = Math.floor(Math.random() * 12);
        card.style.order = randomPos;
    });
    startGame();
})();

cards.forEach(card => card.addEventListener('click', flipCard));